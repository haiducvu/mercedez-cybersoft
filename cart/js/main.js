let productList = [];
let cart = [];

const getEle = (id) => {
  return document.getElementById(id);
};

// set local storage
const setLocalStorage = () => {
  localStorage.setItem("cart", JSON.stringify(cart));
};

// get local storage
const getLocalStorage = () => {
  if (localStorage.getItem("cart")) {
    cart = JSON.parse(localStorage.getItem("cart"));
    renderCart();
  }
};

// function 1: Lấy ds sản phẩm từ BE
const fetchProduct = () => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then((res) => {
      productList = res.data;
      renderProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};

// function 2: render product
const renderProduct = (list = productList) => {
  let htmlContent = "";

  for (let item of list) {
    htmlContent += `
      <div class="col-md-4">
        <div class="card p-2 m-2">
          <img height = "300px" src="${item.image}">
          <p>${item.name}</p>
          <p>Hãng: ${item.type}</p>
          <p>Giá: <b>${parseFloat(item.price).toLocaleString()}đ</b></p>
          <button onclick="addToCart(${
            item.id
          })" class="btn btn-success">Add to Cart</button>
        </div>
      </div>
    `;
  }
  getEle("product").innerHTML = htmlContent;
};

// function 3: Sắp xếp product
const sortProductList = () => {
  let value = getEle("sort").value;
  // sắp xếp từ a - z
  if (value === "az") {
    let noSwap = true;
    for (let i = productList.length; i > 0; i--) {
      for (let k = 0; k < i - 1; k++) {
        if (
          productList[k].name.toLowerCase() >
          productList[k + 1].name.toLowerCase()
        ) {
          let temp = productList[k];
          productList[k] = productList[k + 1];
          productList[k + 1] = temp;
          noSwap = false;
        }
      }
      if (noSwap) {
        break;
      }
    }
    renderProduct();
  }
  // sắp xếp từ z - a
  else if (value === "za") {
    let noSwap = true;
    for (let i = productList.length; i > 0; i--) {
      for (let k = 0; k < i - 1; k++) {
        if (
          productList[k].name.toLowerCase() <
          productList[k + 1].name.toLowerCase()
        ) {
          let temp = productList[k];
          productList[k] = productList[k + 1];
          productList[k + 1] = temp;
          noSwap = false;
        }
      }
      if (noSwap) {
        break;
      }
    }
    renderProduct();
  }
};

// function 4: product filter
const productFilter = () => {
  let value = getEle("filter").value;

  // Lọc samsung
  if (value === "samsung") {
    let samsungList = [];
    for (let item of productList) {
      if (item.type.toLowerCase() === "samsung") {
        samsungList.push(item);
      }
    }
    renderProduct(samsungList);
  }

  // Lọc iphone
  else if (value === "iphone") {
    let iphoneList = [];
    for (let item of productList) {
      if (item.type.toLowerCase() === "iphone") {
        iphoneList.push(item);
      }
    }
    renderProduct(iphoneList);
  }

  // Ko lọc
  else {
    renderProduct();
  }
};

const findIndexProduct = (id) => {
  for (let i = 0; i < productList.length; i++) {
    if (+productList[i].id === id) {
      return i;
    }
  }
  return -1;
};

const findIndexCart = (id) => {
  for (let i = 0; i < cart.length; i++) {
    if (+cart[i].product.id === id) {
      return i;
    }
  }
  return -1;
};

// function 5: add to cart
const addToCart = (id) => {
  let indexProduct = findIndexProduct(id);
  let indexCart = findIndexCart(id);
  let productCart;

  if (indexCart >= 0) {
    cart[indexCart].quantity++;
  } else {
    productCart = new Cart(productList[indexProduct], 1);
    cart.push(productCart);
  }
  renderCart();
};

// function 6: render cart
const renderCart = () => {
  let contentHtml = "";

  for (let item of cart) {
    contentHtml += `
      <tr>
        <td><img src="${item.product.image}" height="50px"></td>
        <td>${item.product.name}</td>
        <td>${parseFloat(item.product.price).toLocaleString()}đ</td>
        <td>${item.quantity}
          <div class="btn-group">
            <button onclick="increaseItem(${
              item.product.id
            })" class="btn btn-info border-right">+</button>
            <button onclick="decreaseItem(${
              item.product.id
            })" class="btn btn-info border-left">-</button>
          </div>
        </td>
        <td>${(
          parseFloat(item.product.price) * parseFloat(item.quantity)
        ).toLocaleString()}đ</td> 
        <td><button onclick="deleteCartItem(${
          item.product.id
        })" class="btn btn-danger">x</button></td> 
      </tr>
    `;
  }

  getEle("tbody-cart").innerHTML = contentHtml;
  getEle("totalPrice").innerHTML = `${calcSum().toLocaleString()}đ`;
  setLocalStorage();
};

// fuction 7: Tăng số lượng
const increaseItem = (id) => {
  let index = findIndexCart(id);
  if (index >= 0) {
    cart[index].quantity++;
    renderCart();
  }
  return false;
};

// function 8: Giảm số lượng
const decreaseItem = (id) => {
  let index = findIndexCart(id);
  if (index >= 0) {
    if (cart[index].quantity > 1) {
      cart[index].quantity--;
    } else {
      deleteCartItem(id);
    }

    renderCart();
  }
};

// function 9: Xóa sản phẩm
const deleteCartItem = (id) => {
  let index = findIndexCart(id);
  if (index !== -1) {
    cart.splice(index, 1);
  }
  renderCart();
};

//function 10: tính tổng tiền
const calcSum = () => {
  let sumTotal = 0;
  let quantity;
  let price;

  for (let i = 0; i < cart.length; i++) {
    quantity = cart[i].quantity;
    price = cart[i].product.price;
    sumTotal += quantity * price;
  }
  console.log(sumTotal);
  return sumTotal;
};

// function 11: thanh toán
const payCart = () => {
  cart = [];
  localStorage.clear();
  renderCart();
  alert("Thanh toán thành công");
};

getEle("sort").addEventListener("change", sortProductList);

getEle("filter").addEventListener("change", productFilter);

getEle("btn-purchase").addEventListener("click", payCart);

fetchProduct();

getLocalStorage();
