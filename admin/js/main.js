let productList = [];

const getEle = (id) => {
  return document.getElementById(id);
};

// function 1: Lấy dữ liệu từ back end
const fetchProduct = () => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then((res) => {
      productList = res.data;
      renderProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};

// function 2: render product
const renderProduct = () => {
  let contentHtml = "";

  for (let item of productList) {
    contentHtml += `
      <tr>
        <td>${item.name}</td>
        <td>${item.type}</td>
        <td>${item.id}</td>
        <td>${item.description}</td>
        <td>${parseFloat(item.price).toLocaleString()}</td>
        <td>${item.rating}</td>
        <td>${item.inventory}</td>
        <td><img src="${item.image}" height="50px"></td>
        <td><button onclick="editProduct(${
          item.id
        })" class="btn btn-success" >Edit</button></td>
        <td><button class="btn btn-danger" onclick="deleteProduct(${
          item.id
        })">Delete</button></td>
      </tr>
    `;
  }
  getEle("tbody").innerHTML = contentHtml;
};

// function 3: add product
const addProduct = () => {
  // Lấy dữ liệu người dùng nhập
  const name = getEle("name").value;
  const type = getEle("type").value;
  const id = getEle("id").value;
  const description = getEle("description").value;
  const price = getEle("price").value;
  const rating = getEle("rating").value;
  const inventory = getEle("inventory").value;
  const image = getEle("image").value;

  // Tạo đối tượng sản phẩm từ thông tin người dùng nhập
  const newProduct = new Product(
    name,
    type,
    id,
    image,
    description,
    price,
    rating,
    inventory
  );

  // call api tới BE thêm nhân viên mới
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "POST",
    data: newProduct,
  })
    .then((res) => {
      fetchProduct();
    })
    .catch((err) => {
      console.log(err);
    });

  getEle("formProduct").reset();
};

// function 4: delete product
const deleteProduct = (id) => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    method: "DELETE",
  })
    .then((res) => {
      fetchProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};

// function 5: get product by id
const getProductById = (id) => {
  let product;

  for (let item of productList) {
    if (+item.id === id) {
      product = item;
      return product;
    }
  }
};

// function 6: edit product
const editProduct = (id) => {
  let product = getProductById(id);

  // đổ dữ liệu vào form
  getEle("name").value = product.name;
  getEle("type").value = product.type;
  getEle("id").value = product.id;
  getEle("image").value = product.image;
  getEle("description").value = product.description;
  getEle("price").value = product.price;
  getEle("rating").value = product.rating;
  getEle("inventory").value = product.inventory;

  getEle("btn-addProduct").style.display = "none";
  getEle("btn-updateProduct").style.display = "inline";
  getEle("btn-cancel").style.display = "inline";
  getEle("id").setAttribute("readonly", true);
};

// function 7: update product
const updateProduct = () => {
  let name = getEle("name").value;
  let type = getEle("type").value;
  let id = getEle("id").value;
  let image = getEle("image").value;
  let description = getEle("description").value;
  let price = getEle("price").value;
  let rating = getEle("rating").value;
  let inventory = getEle("inventory").value;

  let product = new Product(
    name,
    type,
    id,
    image,
    description,
    price,
    rating,
    inventory
  );

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    method: "PUT",
    data: product,
  })
    .then((res) => {
      fetchProduct();
    })
    .catch((err) => {
      console.log(err);
    });

  getEle("btn-addProduct").style.display = "inline";
  getEle("btn-updateProduct").style.display = "none";
  getEle("btn-cancel").style.display = "none";
  getEle("formProduct").reset();
  getEle("id").removeAttribute("readonly");
};

// nút cancel
const cancel = () => {
  getEle("btn-addProduct").style.display = "inline";
  getEle("btn-updateProduct").style.display = "none";
  getEle("btn-cancel").style.display = "none";
  getEle("formProduct").reset();
  getEle("id").removeAttribute("readonly");
};

getEle("btn-updateProduct").addEventListener("click", updateProduct);

getEle("btn-cancel").addEventListener("click", cancel);

getEle("btn-addProduct").addEventListener("click", addProduct);

fetchProduct();
