class Product {
  constructor(name, type, id, image, description, price, rating, inventory) {
    this.name = name;
    this.type = type;
    this.id = id;
    this.image = image;
    this.description = description;
    this.price = price;
    this.rating = rating;
    this.inventory = inventory;
  }
}
